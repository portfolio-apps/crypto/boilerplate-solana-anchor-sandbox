use anchor_lang::prelude::*;

declare_id!("CYWFJd6XKNmNfkM3SGMfynmfa3kT3NsEXNa3UbydZXxS");

#[program]
pub mod organization {
    use super::*;

    pub fn initialize(ctx: Context<Initialize>) -> Result<()> {
        let base_account = &mut ctx.accounts.base_account;
        base_account.total_staff = 0;
        Ok(())
    }

    // Another function woo!
    pub fn add_employee(
        ctx: Context<AddEmployee>, 
        email: String,
        user_address: Pubkey,
        salary: u64
    ) -> Result <()> {
        let base_account = &mut ctx.accounts.base_account;
        let clock = Clock::get().unwrap();

        let item = Employee {
            email: email.to_string(),
            user_address: user_address,
            salary: salary,
            created: clock.unix_timestamp
        };

        base_account.employees.push(item);
        base_account.total_staff += 1;
        Ok(())
    }

    pub fn remove_employee(
        ctx: Context<RemoveEmployee>,
        user_address: Pubkey,
    ) -> Result <()> {
        let base_account = &mut ctx.accounts.base_account;
        let employees = &base_account.employees;

        let index = employees.iter().position(|x| x.user_address == user_address).unwrap();
        
        base_account.employees.remove(index);
        base_account.total_staff -= 1;
        Ok(())
    }
}

#[derive(Accounts)]
pub struct Initialize<'info> {
    #[account(init, payer = user, space = Employee::LEN)]
    pub base_account: Account<'info, BaseAccount>,
    #[account(mut)]
    pub user: Signer<'info>,
    pub system_program: Program <'info, System>,
}

#[derive(Accounts)]
pub struct AddEmployee<'info> {
    #[account(mut)]
    pub base_account: Account<'info, BaseAccount>,
    #[account(mut)]
    pub user: Signer<'info>,
}

#[derive(Accounts)]
pub struct RemoveEmployee<'info> {
    #[account(mut)]
    pub base_account: Account<'info, BaseAccount>,
    #[account(mut)]
    pub user: Signer<'info>,
}

#[account]
pub struct BaseAccount {
    pub total_staff: u64,
    pub employees: Vec<Employee>,
}

#[derive(Debug, Clone, AnchorSerialize, AnchorDeserialize)]
pub struct Employee {
    pub email: String,
    pub user_address: Pubkey,
    pub salary: u64,
    pub created: i64,
}

const DISCRIMINATOR_LENGTH: usize = 8;
const PUBLIC_KEY_LENGTH: usize = 32;
const TIMESTAMP_LENGTH: usize = 8;
const SALARY_LENGTH: usize = 8;
const STRING_LENGTH_PREFIX: usize = 4;
const MAX_EMAIL_LENGTH: usize = 20 * 4;

impl Employee {
    const LEN: usize = DISCRIMINATOR_LENGTH
        + PUBLIC_KEY_LENGTH // Author.
        + TIMESTAMP_LENGTH // Timestamp.
        + SALARY_LENGTH
        + STRING_LENGTH_PREFIX + MAX_EMAIL_LENGTH
        * 10; // How many can be stored in this contract
}