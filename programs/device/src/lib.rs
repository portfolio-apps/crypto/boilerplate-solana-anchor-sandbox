use anchor_lang::prelude::*;

declare_id!("CsvAPhe9Acrz2gN3iEodbeYY42vQLnfyKxMnikn9pFyq");

#[program]
pub mod device {
    use super::*;

    pub fn initialize(ctx: Context<Initialize>) -> Result<()> {
        let base_account = &mut ctx.accounts.base_account;
        base_account.total_devices = 0;
        Ok(())
    }

    // Another function woo!
    pub fn add_device(
        ctx: Context<AddDevice>, 
        ipv6_hash: String,
        authority: Pubkey
    ) -> Result <()> {
        let base_account = &mut ctx.accounts.base_account;
        let clock = Clock::get().unwrap();

        let device = Device {
            ipv6_hash: ipv6_hash.to_string(),
            authority: authority,
            created: clock.unix_timestamp
        };

        base_account.devices.push(device);
        base_account.total_devices += 1;
        Ok(())
    }

    pub fn remove_device(
        ctx: Context<RemoveDevice>,
        authority: Pubkey,
    ) -> Result <()> {
        let base_account = &mut ctx.accounts.base_account;
        let devices = &base_account.devices;

        let index = devices.iter().position(|x| x.authority == authority).unwrap();
        
        base_account.devices.remove(index);
        base_account.total_devices -= 1;
        Ok(())
    }
}

#[derive(Accounts)]
pub struct Initialize<'info> {
    #[account(init, payer = user, space = Device::LEN)]
    pub base_account: Account<'info, BaseAccount>,
    #[account(mut)]
    pub user: Signer<'info>,
    pub system_program: Program <'info, System>,
}

#[derive(Accounts)]
pub struct AddDevice<'info> {
    #[account(mut)]
    pub base_account: Account<'info, BaseAccount>,
    #[account(mut)]
    pub user: Signer<'info>,
}

#[derive(Accounts)]
pub struct RemoveDevice<'info> {
    #[account(mut)]
    pub base_account: Account<'info, BaseAccount>,
    #[account(mut)]
    pub user: Signer<'info>,
}

#[account]
pub struct BaseAccount {
    pub total_devices: u64,
    pub devices: Vec<Device>,
}

#[derive(Debug, Clone, AnchorSerialize, AnchorDeserialize)]
pub struct Device {
    pub ipv6_hash: String,
    pub authority: Pubkey,
    pub created: i64,
}

const DISCRIMINATOR_LENGTH: usize = 8;
const PUBLIC_KEY_LENGTH: usize = 32;
const TIMESTAMP_LENGTH: usize = 8;
const STRING_LENGTH_PREFIX: usize = 4;
const MAX_IPV6_LENGTH: usize = 64;

impl Device {
    const LEN: usize = DISCRIMINATOR_LENGTH
        + PUBLIC_KEY_LENGTH // Author.
        + TIMESTAMP_LENGTH // Timestamp.
        + STRING_LENGTH_PREFIX + MAX_IPV6_LENGTH
        * 10; // How many can be stored in this contract
}