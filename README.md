# Welcome to the Anchor Solana Sandbox
NOTE: This project is intended to demostrate the development lifecycle of building Solana Programs

### Table of Contents
- [Anchor Book](https://book.anchor-lang.com/introduction/introduction.html)
- [Buildspace Dapp Resource](https://buildspace.so/p/build-solana-web3-app)
- [Twitter Clone Dapp](https://lorisleiva.com/create-a-solana-dapp-from-scratch)
- [Solana Web3 Client Functions](https://docs.solana.com/developing/clients/jsonrpc-api)

## Adding programs

### Add new folder to programs directory and copy contents of one program in to new folder
![Add program to directory](https://siasky.net/IAB3MVnzxtUE1_r8UYZ-KChNEAoRKV5CyChCRmj9h7uGxA)

### In the lib.rs file of your new program update the program name where relevant
![Update lib.rs file contents](https://siasky.net/FAAPx6OmDivkqZVXxrEvNLnqpjoKs7E_s37BzDxoQappKw)

### Update the Cargo.toml file in your new program directory
![Update lib.rs file contents](https://siasky.net/RADM8caahvoDnYosIv0EM2g41A3fqSyH8I3P-W_rYgeKYA)

### Retrieve the declare_id for our new program
NOTE: Run anchor test (it will fail due to we have a duplicate program ID in Anchor.toml) then run the command below to retrieve the ID for your new program
<pre>
solana address -k target/deploy/PROGRAM_NAME-keypair.json
</pre>

![Get new program publicKey identifier](https://siasky.net/KAB7vcapix73TTx3fLIxGK8ytqoXJtaZSeTfa9__Lt5LqQ)

### Then update the declare_id in your new program and in the program Anchor.toml file
![Update program ID](https://siasky.net/OACx8r0gQkT402asUscZGQgaQ8JN6k5WM69DrTDXvGSfcw)

![Update Anchor.toml](https://siasky.net/OAAsEHMwRHzyx20QJAbjLFxGTVWSoUZrWtVOnOoE6AR4gQ)

### Create a test file for your new program at ./tests/PROGRAM_NAME.ts and update the test file contents where relevant
![Update Test File Contents](https://siasky.net/dAAy3mpYz_ctnveRsHhR7xgfUkSxjTRM_S4i53O_txV1tw)

### Re-run "anchor test" and you should see them now pass.
![Run Anchor Test](https://siasky.net/IABgmIsYPtFP-9PRaw_JAFHedu5LlfLkibpHIzRCz485EA)

## Deploy to DEVNET

<b>Change env in program Anchor.toml file from "localnet" to "devnet" then run</b>

Note: If you don't have any SOL in your local account see the ./scripts/airdrop.sh file

### Run to deploy
<pre>
anchor deploy
</pre>
![Prepare for devnet deploy](https://siasky.net/JADLtLQNjVzLpVORiHFXriZx4WfhDgMR8iMmNH90moBt7A)
