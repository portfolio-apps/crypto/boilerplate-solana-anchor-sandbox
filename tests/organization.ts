import * as anchor from "@project-serum/anchor";
import { Program } from "@project-serum/anchor";
import { Organization } from "../target/types/organization";

export function createConnection(): anchor.web3.Connection {
  const connection = new anchor.web3.Connection(anchor.web3.clusterApiUrl('devnet'), 'confirmed');
  return connection;
}

const connection = createConnection();

describe("Organization", () => {
  // Configure the client to use the local cluster.
  const provider = anchor.Provider.env()
  anchor.setProvider(provider);

  const program = anchor.workspace.Organization as Program<Organization>;

  const baseAccount = anchor.web3.Keypair.generate();

  const randomAccount = anchor.web3.Keypair.generate();

  it.skip("Is initialized!", async () => {
    // Add your test here.
    const tx = await program.rpc.initialize({
      accounts: {
        baseAccount: baseAccount.publicKey,
        user: provider.wallet.publicKey,
        systemProgram: anchor.web3.SystemProgram.programId
      },
      signers: [baseAccount]
    });
    const balance = await connection.getBalance(provider.wallet.publicKey) / anchor.web3.LAMPORTS_PER_SOL;
    console.log("User balance: ", balance);
    const data = await program.account.baseAccount.fetch(baseAccount.publicKey);
    console.log(data);
  });

  it.skip("Add employee", async () => {
    // Add your test here.
    const tx = await program.rpc.addEmployee(
      'john@test.com',
      randomAccount.publicKey,
      new anchor.BN(100000),
      {
        accounts: {
          baseAccount: baseAccount.publicKey,
          user: provider.wallet.publicKey
        }
      }
    );

    const tx2 = await program.rpc.addEmployee(
      'henry@test.com',
      randomAccount.publicKey,
      new anchor.BN(100000),
      {
        accounts: {
          baseAccount: baseAccount.publicKey,
          user: provider.wallet.publicKey
        }
      }
    );
    
    const data = await program.account.baseAccount.fetch(baseAccount.publicKey);
    console.log(data.employees);
  });

  

  it.skip("Remove Employee", async () => {
    // Add your test here.
    const tx = await program.rpc.removeEmployee(randomAccount.publicKey, {
      accounts: {
        baseAccount: baseAccount.publicKey,
        user: provider.wallet.publicKey
      }
    });
    
    const data = await program.account.baseAccount.fetch(baseAccount.publicKey);
    console.log(data);
  });
});
