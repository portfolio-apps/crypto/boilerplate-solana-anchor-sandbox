import * as anchor from "@project-serum/anchor";
import { Program } from "@project-serum/anchor";
import { Device } from "../target/types/device";

export function createConnection(): anchor.web3.Connection {
  const connection = new anchor.web3.Connection(anchor.web3.clusterApiUrl('devnet'), 'confirmed');
  return connection;
}

const connection = createConnection();

describe("Device", () => {
  // Configure the client to use the local cluster.
  const provider = anchor.Provider.env()
  anchor.setProvider(provider);

  const program = anchor.workspace.Device as Program<Device>;

  const baseAccount = anchor.web3.Keypair.generate();

  const randomAccount = anchor.web3.Keypair.generate();

  it("Is initialized!", async () => {
    // Add your test here.
    const tx = await program.rpc.initialize({
      accounts: {
        baseAccount: baseAccount.publicKey,
        user: provider.wallet.publicKey,
        systemProgram: anchor.web3.SystemProgram.programId
      },
      signers: [baseAccount]
    });
    const balance = await connection.getBalance(provider.wallet.publicKey) / anchor.web3.LAMPORTS_PER_SOL;
    console.log("User balance: ", balance);
    const data = await program.account.baseAccount.fetch(baseAccount.publicKey);
    console.log(data);
  });

  it("Add device", async () => {

    const tx2 = await program.rpc.addDevice(
      'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855',
      randomAccount.publicKey,
      {
        accounts: {
          baseAccount: baseAccount.publicKey,
          user: provider.wallet.publicKey
        }
      }
    );

    const tx3 = await program.rpc.addDevice(
      '4f626e5d6ae6071db98708586cf135999b665bf6230cba5278b07f156238697f',
      randomAccount.publicKey,
      {
        accounts: {
          baseAccount: baseAccount.publicKey,
          user: provider.wallet.publicKey
        }
      }
    );
    
    const data = await program.account.baseAccount.fetch(baseAccount.publicKey);
    console.log(data.devices);
  });

  

  it("Remove device", async () => {
    // Add your test here.
    const tx = await program.rpc.removeDevice(randomAccount.publicKey, {
      accounts: {
        baseAccount: baseAccount.publicKey,
        user: provider.wallet.publicKey
      }
    });
    
    const data = await program.account.baseAccount.fetch(baseAccount.publicKey);
    console.log(data);
  });
});
